# Downloads

## Postman Collections

Below is the list of postman collections.

| Title                           | link                                                                                                                                 |
|---------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| Ozone Open Banking - Build-55   | [Ozone Open Banking - Build-55.postman_collection.json](../../assets/postman/Ozone Open Banking - Build-55.postman_collection.json)  [link](url)   |
| Tide Mocks                      | [Tide Mocks.postman_collection.json](../assets/postman/Tide Mocks.postman_collection.json)                                           |
| Tide PSD2 API Sandbox - Build 1 | [Tide PSD2 API Sandbox - Build 1.postman_collection.json](../assets/postman/Tide PSD2 API Sandbox - Build 1.postman_collection.json) |
